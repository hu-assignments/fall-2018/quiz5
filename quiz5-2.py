import sys
n = int(sys.argv[1])
stars = [star for star in [" "*(n-i) + "*"*(2*i-1) for i in range(1, n+1)]]
stars.extend(stars[n-2:-n-1:-1])
print("\n".join(stars))
