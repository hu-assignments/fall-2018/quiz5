import sys


def print_recursive(num: int, i: int, is_lower: bool):
    if is_lower:
        if i == 0:
            return
        else:
            print(" " * abs(num - i) + "*"*(2*i-1))
            print_recursive(num, i - 1, True)
    else:
        if i == num + 1:
            print_recursive(num, i - 2, True)
        else:
            print(" " * abs(num - i) + "*"*(2*i-1))
            print_recursive(num, i + 1, False)


def diamond(num: int):
    if num > 0:
        print_recursive(num, 1, False)


diamond(int(sys.argv[1]))
